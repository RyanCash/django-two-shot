from django.shortcuts import render
from receipts.models import Receipt
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def receipt_list(request):
    user = request.user
    receipts = Receipt.objects.filter(purchaser=user)
    receipt = Receipt.objects.all()

    context = {
        "receipts": receipt,
    }
    return render(request, "receipts/list.html", context)
