from django.urls import path
from accounts.views import user_login, user_logout
from django.contrib.auth import views as auth_views

urlpatterns = [
    path("logout/", user_logout, name="logout"),
    path("login/", user_login, name="login"),
    path(
        "login/",
        auth_views.LoginView.as_view(template_name="login.html"),
        name="login",
    ),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
]
